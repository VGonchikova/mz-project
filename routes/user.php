<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\MatchController;
use App\Http\Controllers\User\TestController;

Route::prefix('user')->group(function (){
    Route::get('/match',[MatchController::class,'index'])->name('user.match');
    Route::post('/match',[MatchController::class,'store'])->name('user.match.store');
    Route::get('/test',[TestController::class,'index'])->name('user.test');
    Route::post('/test',[TestController::class,'store'])->name('user.test.store');
});

